
var ObjectId = require('mongodb').ObjectID;

exports.user_test = function(db){
    var time = parseInt(new Date().getTime()/1000);
    db.collection("users").deleteMany({"test": 1}, function(err, result){
        //console.log(result);
    });
    var user = {
        "test": 1,
        "email" : "test1@test.test",
        "created_at" : time,
        "email_trusted" : 0,
        "proof" : 0,
        "rating" : 0.0,
        "Location" : {
            "lat" : "57.04094420000001",
            "lng" : "53.97693030000005",
            "place_id" : "ChIJCXEJ-jTE5kMRLXNAC4oln2E",
            "addr" : "Воткинск, Удмуртская Республика, Россия"
        },
        "updated_at" : 1516882405,
        "account_verification" : 0,
        "email_trusted" : 0,
        "currentLang" : 94,
        "prefLang" : 24,
        "first_name" : "Наталья1",
        "last_name" : "Плешакова1",
        "password" : "123",
        "type_profile" : "Student",
        "Student" : {
            "ready_to_work" : 1,
            "gender" : 0,
            "prefLang" : 94
        },
        "Settings" : {
            "news" : true,
            "contact_info" : false,
            "progress_in_edu" : true,
            "progress_in_edu2" : true,
            "progress_in_edu12" : false,
            "progress_in_edu233" : false,
            "news223" : false
        },
        "birthday" : 735422400,
        "displayAs" : 1,
        "Photo" : ObjectId("5a69cadb6d644a08f7868024")
    };
    user.email_trusted =
    db.collection("users").insertMany([
        {
            "test": 1,
            "email" : "test1@test.test",
            "created_at" : time,
            "email_trusted" : 0,
            "proof" : 0,
            "rating" : 0.0,
            "Location" : {
                "lat" : "57.04094420000001",
                "lng" : "53.97693030000005",
                "place_id" : "ChIJCXEJ-jTE5kMRLXNAC4oln2E",
                "addr" : "Воткинск, Удмуртская Республика, Россия"
            },
            "updated_at" : 1516882405,
            "account_verification" : 0,
            "email_trusted" : 0,
            "currentLang" : 94,
            "prefLang" : 24,
            "first_name" : "Наталья1",
            "last_name" : "Плешакова1",
            "password" : "123",
            "type_profile" : "Student",
            "Student" : {
                "ready_to_work" : 1,
                "gender" : 0,
                "prefLang" : 94
            },
            "Settings" : {
                "news" : true,
                "contact_info" : false,
                "progress_in_edu" : true,
                "progress_in_edu2" : true,
                "progress_in_edu12" : false,
                "progress_in_edu233" : false,
                "news223" : false
            },
            "birthday" : 735422400,
            "displayAs" : 1,
            "Photo" : ObjectId("5a69cadb6d644a08f7868024")
        }
        ]);

};
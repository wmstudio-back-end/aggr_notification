var ObjectId = require('mongodb').ObjectID;


function notification(db, users_id, options_notification,callback) {
    var time = parseInt(new Date().getTime()/1000);
    db.collection("notification").find({"users_id": ObjectId(users_id), "options_notification": options_notification}).count(function (err, count) {
        if(count === 0){
            var send = {
                "users_id": ObjectId(users_id),
                "options_notification": options_notification,
                "created_at": time
            };
            db.collection("notification").insertMany([send],function(err,data){
                callback(true)
            });
            console.log("notification("+send.users_id+"): "+send.options_notification);
        }else{
            callback(true)
        }
    });
}

// Registration 2, 3, 7
exports.registration = function (db, users, callback) {
    var error_code = {
        "Student": 2,
        "Company": 3,
        "Educator": 7
        };
    Promise.all(users.map(function (user) {
            return new Promise((resolve) => {
                notification(db, user._id, error_code[user.type_profile],resolve);
            });
        })).then(() => callback(null,true));

    



}

//Account Verification 8, 9
exports.accountVerification = function (db, users, callback) {
    var error_code = {
        "Student": 8,
        //"Company": 3,
        "Educator": 9
    };

    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// CV Photo 13; Company logo missing 30; Educator photo 34
exports.photo = function (db, users, callback) {
    var error_code = {
        "Student": 13,
        "Company": 30,
        "Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

//User Data Fill 10
exports.userDataFill = function (db, users, callback) {
    var error_code = {
        "Student": 10,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

//CV Create 11
exports.resume = function (db, users, callback) {
    var error_code = {
        "Student": 11,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

//CV Fill 12
exports.resumeFill = function (db, users, callback) {
    var error_code = {
        "Student": 12,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Education Fill 14
exports.educationFill = function (db, users, callback) {
    var error_code = {
        "Student": 14,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Grades/Progress in education Fill 15
exports.progressEducationFill = function (db, users, callback) {
    var error_code = {
        "Student": 15,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Grades/Progress in education Confirm 16
exports.progressEducationConfirm = function (db, users, callback) {
    var error_code = {
        "Student": 16,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Recommendation request 17
exports.recommendationRequest = function (db, users, callback) {
    var error_code = {
        "Student": 17,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Search saving CTA
exports.searchSavingCTA = function (db, users, callback) {
    var error_code = {
        "Student": 18,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// New message
exports.newMessage = function (db, users, callback) {
    var error_code = {
        "Student": 19,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

//Bookmarked vacancy deadline reminder 20
exports.bookmarkedDeadline = function (db, users, callback) {
    var error_code = {
        "Student": 20,
        //"Company": 30,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

//Rate Company CTA 22
exports.rateCompanyCTA = function (db, users, callback) {
    var error_code = {
        "Student": 22,
        //"Company": 22,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Invoice payment deadline notice 23
exports.invoicePaymentNotice = function (db, users, callback) {
    var error_code = {
        //"Student": 20,
        "Company": 23,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Invoice payment deadline passed 24
exports.invoicePaymentPassed = function (db, users, callback) {
    var error_code = {
        //"Student": 20,
        "Company": 24,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Subscription end notification 26
exports.subscriptionEndNotification = function (db, users, callback) {
    var error_code = {
        //"Student": 20,
        "Company": 26,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Vacancy is not active 27
exports.vacancyIsNotActive = function (db, users, callback) {
    var error_code = {
        //"Student": 20,
        "Company": 27,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Company information missing 29
exports.companyInformationMissing = function (db, users, callback) {
    var error_code = {
        //"Student": 20,
        "Company": 29,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

//No Company comment 31
exports.noCompanyComment = function (db, users, callback) {
    var error_code = {
        //"Student": 20,
        "Company": 31,
        //"Educator": 34
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// New recommendation request 32
exports.newRecommendationRequest = function (db, users, callback) {
    var error_code = {
        //"Student": 20,
        //"Company": 32,
        "Educator": 32
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

// Give recommendation request 33
exports.giveRecommendationRequest = function (db, users, callback) {
    var error_code = {
        //"Student": 20,
        //"Company": 33,
        "Educator": 33
    };
    Promise.all(users.map(function (user) {
        return new Promise((resolve) => {
            notification(db, user._id, error_code[user.type_profile],resolve);
        });
    })).then(() => callback(null,true));
}

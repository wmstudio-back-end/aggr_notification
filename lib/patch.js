
exports.users_created_at = function (db) {

    var time = parseInt(new Date().getTime()/1000);
    db.collection("users").updateMany({"created_at": null},{$set: {"created_at": time-3600*24*10}});
    console.log("TIME: "+time);
}

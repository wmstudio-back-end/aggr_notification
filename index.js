/*
реализованые события: 2, 3, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 26, 27, 29, 30, 31, 32, 33, 34,
НЕ реализованые события: 4, 5, 6, 21, 25, 28, 35, 36
*/
argv = require('minimist')(process.argv.slice(2));
console.log(argv)

/*
 Seconds: 0-59
 Minutes: 0-59
 Hours: 0-23
 Day of Month: 1-31
 Months: 0-11
 Day of Week: 0-6
* */
var CronJob = require('cron').CronJob;
var job = new CronJob({

    cronTime: '* * * 12 * *',

    onTick: startNotyfy,
    start: false,
    timeZone: 'America/Los_Angeles'
});
 job.start();

var mongoClient = require("mongodb").MongoClient;
var ObjectId = require('mongodb').ObjectID;
var notification = require('./lib/notification.js');
var patch = require('./lib/patch.js');
var url = 'mongodb://project:project@localhost:27017/project';
var async = require('async');
db = null
mongoClient.connect(url, function(err, ldb) {
    db = ldb
})
function startNotyfy(){

      console.log('START')
        // clearTimeout(timeoutQueue);
        // timeoutQueue = setTimeout(hung, 3000);
        // хрень
        // patch.users_created_at(db);
        async.series([
            function (callback) {
                // Registration 2, 3, 7
                db.collection("users").find({
                    "email_trusted": {$ne: 1},
                    "first_name" : {$ne:null},
                    "type_profile": {$in: ["Student", "Company", "Educator"]}
                }, {"_id": 1, "type_profile": 1}).toArray(function (err, results) {
                    notification.registration(db, results,callback);
                });
            },
            function(callback){
                //Account Verification 8, 9
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("users").find({
                    "account_verification": {$ne: 1},
                    "first_name" : {$ne:null},
                    "type_profile": {$in: ["Student", "Educator"]},
                    "created_at": {$lte: time - 3600 * 24 * 2}
                }, {"_id": 1, "type_profile": 1}).toArray(function (err, results) {
                    notification.accountVerification(db, results,callback);
                });
            },
            function(callback){
                // CV Photo 13; Company logo missing 30; Educator photo 34
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("users").find({
                    "Photo": null,
                    "first_name" : {$ne:null},
                    "type_profile": {$in: ["Student", "Company", "Educator"]},
                    "created_at": {$lte: time - 3600 * 24 * 3}
                }, {"_id": 1, "type_profile": 1}).toArray(function (err, results) {
                    notification.photo(db, results, callback);
                });
            },
            function(callback){

                //User Data Fill 10
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("users").find({
                    "type_profile": "Student", "created_at": {$lte: time - 3600 * 24 * 3},
                    $or: [{"phone": null}, {"currentLang": null}, {"personalId": null}, {"birthday": null}]
                }, {"_id": 1, "type_profile": 1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("resume").find({"uid": value._id}).count(function (err, count) {
                                if (count > 0) {
                                    notification.userDataFill(db, [{"_id": value._id, "type_profile": value.type_profile}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){
                console.log('000000000000')
                //CV Create 11
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("users").find({"type_profile": "Student", "created_at": {$lte: time - 3600 * 24 * 1}}, {
                    "_id": 1,
                    "type_profile": 1
                }).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("resume").find({"uid": value._id}).count(function (err, count) {
                                if (count === 0) {
                                    notification.resume(db, [{"_id": value._id, "type_profile": value.type_profile}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){


                //CV Fill 12
                var time = parseInt(new Date().getTime() / 1000);
                db.collection('resume').find(
                    {
                        "created_at": {$lte: time - 3600 * 24 * 4},
                        $or: [
                            {"objective": {$exists: false}},
                            {"languageSkills": {$exists: false}},
                            {"languageSkills": {$size: 0}},
                            {"languageSkills.langLevel": {$exists: false}},
                            {"languageSkills.langLevel": {$ne: 6}},
                            {"workExp": {$exists: false}},
                            {"workExp": {$size: 0}},
                            {"strenghs": {$exists: false}},
                            {"characterTraits": {$exists: false}},
                            {"interestsAndHobbies": {$exists: false}},
                            {"yourMainValues": {$exists: false}},
                            {"internshipOptions.init": {$exists: false}},
                            {"internshipOptions.possible_internship": {$exists: false}},
                            {"internshipOptions.internshipType": {$exists: false}},
                            {"internshipOptions.occupationField": {$exists: false}},
                            {"internshipOptions.locations": {$exists: false}},
                            {"internshipOptions.preferredWorkTime": {$exists: false}},
                            {"internshipOptions.additonalInformation": {$exists: false}},
                            {"jobOptions.jobType": {$exists: false}},
                            {"jobOptions.occupationField": {$exists: false}},
                            {"jobOptions.possible_position": {$exists: false}},
                            {"jobOptions.preferredWorkTime": {$exists: false}},
                            {
                                $and: [
                                    {"jobOptions.locations": {$exists: false}},
                                    {"jobOptions.remote_work": false}
                                ]
                            },
                            {"additional": {$exists: false}}
                        ]
                    },
                    {"uid": 1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            notification.resumeFill(db, [{"_id": value.uid, "type_profile": "Student"}],resolve);
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){


                // Education Fill 14
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("resume").find({
                        "created_at": {$lte: time - 3600 * 24 * 2}},
                    {"uid": 1, "type_profile": 1}).toArray(function (err, results) {

                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("school_user_data").find({"uid": value.uid}).count(function (err, count) {
                                if (count === 0) {
                                    notification.educationFill(db, [{"_id": value.uid, "type_profile": "Student"}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));


                });
            },
            function(callback){

                // Grades/Progress in education Fill 15
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("resume").find({"created_at": {$lte: time - 3600 * 24 * 4}}, {"uid": 1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("school_user_data").find({"uid": value.uid, "SchoolProgressEducation.created_at": {$exists: false}}).count(function (err, count) {
                                if (count === 0) {
                                    notification.progressEducationFill(db, [{"_id": value.uid, "type_profile": "Student"}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){

                // Grades/Progress in education Confirm 16
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("school_user_data").find({"SchoolProgressEducation.created_at": {$lte: time - 3600 * 24 * 2},
                    $or: [{"SchoolProgressEducation.status": {$ne: 2}},
                        {"SchoolProgressEducation.file": {$exists: false}}]
                }, {"uid": 1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            notification.progressEducationConfirm(db, [{"_id": value.uid, "type_profile": "Student"}],resolve);
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){

                // Recommendation request 17
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("resume").find({"created_at": {$lte: time - 3600 * 24 * 7}}, {"uid": 1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("recommendation").find({"uuid": value.uid}).count(function (err, count) {
                                if (count === 0) {
                                    notification.recommendationRequest(db, [{"_id": value.uid, "type_profile": "Student"}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){

                //searchSavingCTA 18
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("users").find({"Student.ready_to_work": 1}, {"_id": 1, "type_profile": 1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("savedSearches").find({"uid": value._id}).count(function (err, count) {
                                if (count === 0) {
                                    notification.searchSavingCTA(db, [{"_id": value._id, "type_profile": value.type_profile}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){


                // New message 19
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("messages").find({
                    "date": {$lte: time - 3600 * 24 * 3},
                    "status": 0
                }, {"receiver": 1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("users").find({"_id": value.receiver, "type_profile": "Student"}).count(function (err, count) {
                                if (count === 1) {
                                    notification.newMessage(db, [{"_id": value.receiver, "type_profile": "Student"}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));
                });

            },
            function(callback){


                //Bookmarked vacancy deadline reminder 20
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("vacancy").find({
                    "deadLine": {
                        $lte: time + 3600 * 24 * 5,
                        $gte: time
                    }
                }, {"_id": 1,"deadLine":1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("bookmarks").find({"items.resumeId": ObjectId(value._id)}, {"uid": 1}).toArray(function (err, items) {
                                Promise.all(items.map(function (item) {
                                    return new Promise((resolve2) => {
                                        notification.bookmarkedDeadline(db, [{"_id": item.uid, "type_profile": "Student"}],resolve2);
                                    });
                                })).then(() => resolve(true));
                            });
                        });
                    })).then(() => callback(null,true));
                });
            },
            function(callback){

                //Rate Company CTA 22
                var time = parseInt(new Date().getTime() / 1000);

                db.collection("users").find({
                    "type_profile": "Company",
                    "Company.auth" : true,
                    "created_at": {$lte: time - 3600 * 24 * 7}
                }, {"_id": 1, "type_profile": 1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("PublicCompanyComments").find({"companyid": value._id}).count(function (err, count) {
                                if (count === 0) {
                                    notification.rateCompanyCTA(db, [{"_id": value._id, "type_profile": value.type_profile}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){

                // Invoice payment deadline notice 23
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("options").find({"_id": ObjectId("59ca0e416e537a165a06ca52")},{"userOrderLife": 1}).toArray(function (err, options) {
                    Promise.all(options.map(function (value) {
                        return new Promise((resolve) => {
                            db.collection("user_order").find({
                                "status": 0,
                                "created_at": {
                                    $lte: time + 3600 * 24 * 1 - option.userOrderLife,
                                    $gte: time - option.userOrderLife
                                }
                            },{"uid": 1}).toArray(function (err, user_orders) {
                                //console.log(user_orders);
                                Promise.all(user_orders.map(function (user_order) {
                                    return new Promise((resolve2) => {
                                        db.collection("users").find({"_id": user_order.uid, "type_profile": "Company"}).toArray(function (err, users) {
                                            Promise.all(users.map(function (user) {
                                                return new Promise((resolve3) => {
                                                    notification.invoicePaymentNotice(db, [{"_id": user._id, "type_profile": user.type_profile}],resolve3);
                                                });
                                            })).then(() => resolve2(true));

                                        });
                                    });
                                })).then(() => resolve(true));

                            });
                        });
                    })).then(() => callback(null,true));

                })
            },
            function(callback){




                // Invoice payment deadline notice 24
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("options").find({"_id": ObjectId("59ca0e416e537a165a06ca52")},{"userOrderLife": 1}).toArray(function (err, options) {
                    Promise.all(options.map(function (option) {
                        return new Promise((resolve) => {
                            db.collection("user_order").find({
                                "status": 0,
                                "created_at": {
                                    $lte: time - 3600 * 24 * 1 ,
                                    $gte: time - option.userOrderLife + 3600 * 24 * 1
                                }
                            },{"uid": 1}).toArray(function (err, user_orders) {
                                Promise.all(user_orders.map(function (user_order) {
                                    return new Promise((resolve2) => {
                                        db.collection("users").find({"_id": user_order.uid, "type_profile": "Company", "Company.auth" : true}).toArray(function (err, users) {
                                            Promise.all(users.map(function (user) {
                                                return new Promise((resolve3) => {
                                                    notification.invoicePaymentPassed(db, [{"_id": user._id, "type_profile": user.type_profile}],resolve3);
                                                });
                                            })).then(() => resolve2(true));

                                        });
                                    });
                                })).then(() => resolve(true));
                            });
                        });
                    })).then(() => callback(null,true));

                })
            },
            function(callback){

                //Subscription end notification 26
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("users").find({
                    "type_profile": "Company",
                    "Company.auth" : true,
                    "service.active.orderId": {$exists: true}
                }, {
                    "_id": 1,
                    "type_profile": 1,
                    "service.active.orderId": 1
                }).toArray(function (err, users) {
                    Promise.all(users.map(function (user) {
                        return new Promise((resolve) => {
                            db.collection("user_order").find({
                                "_id": ObjectId("5a6f079eec777d262d24c26d"),
                                "service.subscription.deactivate_at": {
                                    $lte: time + 3600 * 24 * 5,
                                    $gte: time + 3600 * 24 * 4
                                }
                            }).count(function (err, count) {
                                if(count != 0){
                                    notification.subscriptionEndNotification(db, [{"_id": user._id, "type_profile": user.type_profile}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){

                // Vacancy is not active 27
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("vacancy").find({
                    "deadLine": {
                        $lte: time - 3600 * 24 * 1,
                        $gte: time - 3600 * 24 * 10
                    }
                }, {"uid": 1,"deadLine":1}).toArray(function (err, results) {
                    Promise.all(results.map(function (value) {
                        return new Promise((resolve) => {
                            notification.vacancyIsNotActive(db, [{"_id": value.uid, "type_profile": "Company"}],resolve);
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){



                // Company information missing 29
                var time = parseInt(new Date().getTime() / 1000);
                db.collection("users").find({
                    "type_profile": "Company",
                    "Company.auth" : true,
                    $or: [
                        {"Company.phone": {$exists: false}},
                        {"Company.Address": {$exists: false}},
                        {"Company.aboutCompany": {$exists: false}},
                        {"Company.activityField.id": {$exists: false}},
                        {"Company.skills.id": {$exists: false}},
                        {"Company.Established": {$exists: false}},
                        {"Company.companySize": {$exists: false}},
                        {"Company.legalFormBusiness": {$exists: false}},
                    ]}, {
                    "_id": 1,
                    "type_profile": 1
                }).toArray(function (err, users) {
                    Promise.all(users.map(function (user) {
                        return new Promise((resolve) => {
                            notification.companyInformationMissing(db, [{"_id": user._id, "type_profile": user.type_profile}],resolve);
                        });
                    })).then(() => callback(null,true));
                });
            },
            function(callback){

                var time = parseInt(new Date().getTime() / 1000);
                //No Company comment 31
                db.collection("users").find({
                    "type_profile": "Company",
                    "Company.auth" : true,
                    "created_at": {$lte: time - 3600 * 24 * 20}
                }, {
                    "_id": 1,
                    "type_profile": 1
                }).toArray(function (err, users) {
                    Promise.all(users.map(function (user) {
                        return new Promise((resolve) => {
                            db.collection("PublicCompanyComments").find({
                                "companuId": ObjectId(user._id),
                            }).count(function (err, count) {
                                if(count === 0){

                                    notification.noCompanyComment(db, [{"_id": user._id, "type_profile": user.type_profile}],resolve);
                                }else{

                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){

                // New recommendation request 32
                db.collection("recommendation").find({
                    "status": 0
                }, {
                    "educatorId": 1
                }).toArray(function (err, recommendations) {
                    Promise.all(recommendations.map(function (recommendation) {
                        return new Promise((resolve) => {
                            notification.newRecommendationRequest(db, [{"_id": recommendation.educatorId, "type_profile": "Educator"}],resolve);
                        });
                    })).then(() => callback(null,true));

                });
            },
            function(callback){

                var time = parseInt(new Date().getTime() / 1000);
                // Give recommendation request 33
                db.collection("users").find({
                    "type_profile": "Educator",
                    "created_at": {$lte: time - 3600 * 24 * 5}
                }, {
                    "_id": 1,
                    "type_profile": 1
                }).toArray(function (err, users) {
                    Promise.all(users.map(function (user) {
                        return new Promise((resolve) => {
                            db.collection("recommendation").find({
                                "educatorId": ObjectId(user._id)
                            }).count(function (err, count) {
                                if(count === 0){
                                    notification.giveRecommendationRequest(db, [{"_id": user._id, "type_profile": user.type_profile}],resolve);
                                }else{
                                    resolve(true)
                                }
                            });
                        });
                    })).then(() => callback(null,true));

                });
            }
        ],function(err,data){
            console.log('!!!!!!!!!!!!!!!!!!!',err,data)

        })













        // end connect

}

// startNotyfy()
//
// function hung() {
//     console.log('time out!! ');
//     clearTimeout(timeoutQueue);
//     timeoutQueue=setTimeout(hung, 10000);
// }
